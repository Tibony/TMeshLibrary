// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "TMeshLibraryBPLibrary.h"
#include "TMeshLibrary.h"

UTMeshLibraryBPLibrary::UTMeshLibraryBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

float UTMeshLibraryBPLibrary::TMeshLibrarySampleFunction(float Param)
{
	return -1;
}

float UTMeshLibraryBPLibrary::GetSkeletalMeshMorphTarget(USkeletalMesh *SkeletalMesh, FString Name, FTibonyMorphTarget &MorphOut)
{


	
	int32 &index = SkeletalMesh->MorphTargetIndexMap.FindOrAdd(FName(*Name));
	
	int32 Length = SkeletalMesh->MorphTargets[index]->MorphLODModels.Num();

	TArray<FTibonyMorphTargetInfo> Ready111;

	TArray<FMorphTargetDelta>  aaa;

	aaa = SkeletalMesh->MorphTargets[index]->MorphLODModels[0].Vertices;
	
	for (auto& abc : aaa)
	{
		FTibonyMorphTargetInfo local;
		local.Postion = abc.PositionDelta;
		local.Target = abc.TangentZDelta;
		Ready111.Add(local);

	}
	MorphOut.Name = Name;
	MorphOut.TibonyMorphTargetInfo = Ready111;
	return Length;
}
/*
//FMorphTargetLODModel
//TArray<FMorphTargetLODModel> LoopLODModel = SkeletalMesh.MorphTargets[0]->MorphLODModels;
for (auto& abc : aaa)
{
	FTibonyMorphTargetInfo local;
	local.Postion = abc.PositionDelta;
	Ready111.add(local);
}
//TArray<FTibonyMorphTargetInfo> Ready;
//for (auto &Morphdd : LoopLODModel)
//{

//}


//for (TPair<FName, int32>& MapName : SkeletalMesh.MorphTargetIndexMap)
//{
//	FTibonyMorphTargetInfo ReadyAppend;
//	SkeletalMesh.MorphTargets[0]->MorphLODModels[0].Vertices[0].PositionDelta;
//
	//}
	*/
